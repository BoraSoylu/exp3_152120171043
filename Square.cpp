/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 *   Edited by Bora Soylu on 18/11/2020
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double a){
	this->a = a;
}

void Square::setB(double a){
	this->a = a;
}

double Square::calculateCircumference(){
	return (a + a) * 2;
}

double Square::calculateArea(){
	return a * a;
}
