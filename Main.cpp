#include "Triangle.h"
#include "Square.h"
#include "Circle.h"

#include <iostream>
using namespace std;
// I've appended comments to the end of the questions.
/*
 * Question1: Fix the code and compile it */
void test(); //added function declaration

int main() {
	/****************************************************/
	/*DO NOT CHANGE any line of code in this function */
	Triangle triangle(3, 5, 6);
	triangle.setA(7);
	triangle.setB(8);
	triangle.setC(9);
	double triAngleCircumference = triangle.calculateCircumference();

	Circle circle(3);
	double circleCircumference1 = circle.calculateCircumference();
	circle.setR(5);
	double circleCircumference2 = circle.calculateCircumference();
	double circleArea = circle.calculateArea();

	Square square(3);
	double squareCircumference1 = square.calculateCircumference();
	square.setA(4);
	square.setB(5);
	double squareCircumference2 = square.calculateCircumference();
	double squareArea = square.calculateArea();
	/****************************************************/
	test();
	return 0;
}

void test() {
	const Circle circle1(30);
	/* Question2: Block the changing r of above object not other objects only above object.
	Made circle1 a const object and Circle class' getR and calculateCircumference methods conts 
	methods.*/
	//circle1.setR(20); //This line must show compile error.
	double circumference = circle1.calculateCircumference();
	circle1.getR() ;

	/*DO NOT REMOVE below code block*/
	{
		Circle circle2(30);
		circle2.setR(20);
		double circumference2 = circle2.calculateCircumference();
		circle2.getR();
	}
	/*End of code block*/

	Circle circle4(5);
	circle4.setR(10);
	Circle circle5(10);
	/* Question3: In Circle class, create an equals function which returns boolean to compare circle4 and circle5 objects
	 * and print if they are equal or not. I've added the required function.*/

	if (circle4.equals(circle4, circle5)){
		cout << "circle4 equals circle 5.\n";
	}
	else {
		cout << "circle4 does not equals circle 5.\n";
	}

	/* Question4: Review the PI variable and make it unchangeable from anywhere of that program.
	 * You know PI is always 22/7. This one seemed too easy as I only added a const keyword
	 to the PI declaration. I hope I'm not missing anything.*/
	
	/* Question5: Overload the setR method of Circle class to take integer values. I am not sure
	if I did this one correctly.	*/




}

/*
 * Question6: Review the code and fix the bugs
 * 
 * Swapped circumference and area functions in Circle
 * Changed setB to also set a as setA, added pointers, added parentheses to circumference calculation in square.
 * Added pointers and fixed circumference calculation in triangle
 * 
 */

