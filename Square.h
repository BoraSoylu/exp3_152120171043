/*
 * Square.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 *	Edited by Bora Soylu on 18/11/2020
 */

#ifndef SQUARE_H_
#define SQUARE_H_

class Square {
public:
	Square(double);
	virtual ~Square();
	void setA(double);
	void setB(double);
	double calculateCircumference();
	double calculateArea();
private:
	double a;
	double b;
};

#endif /* SQUARE_H_ */
